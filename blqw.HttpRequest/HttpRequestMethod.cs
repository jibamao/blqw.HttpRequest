﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blqw.Web
{
    /// <summary> 表示http请求的方式
    /// </summary>
    public enum HttpRequestMethod
    {
        GET,
        POST,
        HEAD,
        TRACE,
        PUT,
        DELETE,
        OPTIONS,
        CONNECT,
    }
}
